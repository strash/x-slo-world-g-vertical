extends Control


signal change_view_requested


# описание видимости блоков Header
const HEADER_SETUP: Dictionary = {
	"is_header_visible": true,
	"is_score_visible": true,
	"is_level_visible": true,
	"is_btn_back_visible": false,
	"is_bg_visible": true,
}


const TOP_OFFSET: float = 50.0
var boxC: BoxContainer
var page: int = 1 setget set_page
var page_count: int = 0
var button_max_w: float = 0.0


# BUILTINS -------------------------


func _ready() -> void:
	boxC = $Box
	adjust_pages()
	page_count = ($Box as BoxContainer).get_child_count()
	self.page = page


func _notification(what: int) -> void:
	if what == NOTIFICATION_RESIZED and boxC != null:
		adjust_pages()
		self.page = page

# METHODS -------------------------


func adjust_pages() -> void:
	var _view: Vector2 = get_viewport_rect().size
	for i in boxC.get_children():
		if  i.rect_size.x > button_max_w:
			button_max_w = i.rect_size.x
	boxC.set("custom_constants/separation", (_view.x - button_max_w) / 2.0)
	boxC.rect_position.y = (_view.y - boxC.rect_size.y) / 2.0 + TOP_OFFSET
	boxC.rect_size.x = 0.0


# SETGET -------------------------


func set_page(value: int) -> void:
	page = value
	var _index: int = value - 1
	var _view_w: float = get_viewport_rect().size.x
	boxC.rect_position.x = _view_w / 2.0 - button_max_w / 2.0 - button_max_w * _index - boxC.get("custom_constants/separation") * _index


# SIGNALS -------------------------


func _on_BtnLvl1_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 1 })


func _on_BtnLvl2_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 2 })


func _on_BtnLvl3_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 3 })


func _on_BtnLeft_pressed() -> void:
	self.page = page - 1 if page > 1 else page_count


func _on_BtnRight_pressed() -> void:
	self.page = page + 1 if page < page_count else 1


